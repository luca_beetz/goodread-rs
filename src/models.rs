pub struct Book {
    pub id: u32,
    pub title: String,
    pub description: String,
    pub num_pages: Option<u32>,
}

pub struct Author {
    pub id: u32,
    pub name: String,
}