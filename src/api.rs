use hyper::client::{Client, HttpConnector};

use super::models::Book;

pub mod goodreads_api_endpoints {
    pub const SEARCH_BOOKS: &str = "https://www.goodreads.com/search/index.xml";
    pub const SEARCH_AUTHORS: &str = "https://www.goodreads.com/api/author_url/";
}

pub struct GoodreadsApiClient<'a> {
    api_key: &'a str,
    client: Client<HttpConnector>,
}

impl GoodreadsApiClient<'_> {
    pub fn new<'a>(api_key: &'a str) -> GoodreadsApiClient<'_> {
        let client = Client::new();

        GoodreadsApiClient {
            api_key: api_key,
            client: client,
        }
    }

    pub async fn search_books<'a>(&self, title: &'a str) {
        let uri = format!("{}?key={}&q={}", self::goodreads_api_endpoints::SEARCH_BOOKS, self.api_key, title)
            .parse()
            .expect("Unable to parse URI");

        let res = self.client.get(uri).await
            .expect("Unable to fetch response");
    }
}